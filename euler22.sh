#!/bin/sh
"$(dirname "$0")/euler22.sed" < p022_names.txt |
	sort |
	"$(dirname "$0")/euler22.awk"
