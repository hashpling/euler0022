#!/usr/bin/awk -E

BEGIN {
    letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    total = 0
}

{
    sum = 0
    for (idx = 1; idx <= length($1); ++idx) {
        sum += index(letters, substr($1, idx, 1))
    }
    total += NR * sum
}

END {
    print total
}
